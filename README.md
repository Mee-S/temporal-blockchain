## Temporal Blockchain model with ProB

An adaptation of a Temporal "rolling" blockchain model proposed by R. Dennis, G. Owenson and B. Aziz in the paper "A Temporal Blockchain: A Formal Analysis" 


Total states the ProB model checker visited:

![](Images/CheckTotalStates.png) ![](Images/ModelCheck.png)


A graph drawn showing the cryptographic link between the current and previous block hash:

![](Images/cryptoLINK.jpg) 


The invariant holds during execution of the add_block operation:
![](Images/InvariantHoldsAddBLOCK.png) 


This shows an expected invariant violation during execution of an preemptive attempt to delete a block:
![](Images/invariantViolationCardinalityBLOCKDELETE.png) 


A state graph generated in ProB after having added 1 block to the blockchain:
![](Images/StateGraph1.png)







# References

R. Dennis, G. Owenson and B. Aziz, "A Temporal Blockchain: A Formal Analysis," 2016 International Conference on Collaboration Technologies and Systems (CTS), Orlando, FL, 2016, pp. 430-437, doi: 10.1109/CTS.2016.0082.

https://www3.hhu.de/stups/handbook/prob2/prob_handbook.html#summary-of-b-syntax
